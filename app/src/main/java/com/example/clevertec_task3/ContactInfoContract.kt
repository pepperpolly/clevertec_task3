package com.example.clevertec_task3

import android.content.Context
import android.content.Intent
import android.provider.ContactsContract
import androidx.activity.result.contract.ActivityResultContract

class ContactInfoContract :
    ActivityResultContract<Unit, Intent?>() {
    override fun createIntent(context: Context, input: Unit?): Intent {
        return Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
    }


    override fun parseResult(resultCode: Int, intent: Intent?): Intent? {
        return intent
    }

}