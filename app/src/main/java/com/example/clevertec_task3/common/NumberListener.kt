package com.example.clevertec_task3.common

interface NumberListener {
    fun saveNumber(number: String)
}