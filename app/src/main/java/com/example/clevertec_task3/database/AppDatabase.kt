package com.example.clevertec_task3.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [ContactInfo::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun contactsDao(): ContactsDao
}