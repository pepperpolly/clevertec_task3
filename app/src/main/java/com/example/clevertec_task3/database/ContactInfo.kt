package com.example.clevertec_task3.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ContactInfo(
    @PrimaryKey var number: String,
    var name: String?,
    var familyName: String?,
    var emailAddress: String?
)