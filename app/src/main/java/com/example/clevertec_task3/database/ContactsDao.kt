package com.example.clevertec_task3.database

import androidx.room.*

@Dao
interface ContactsDao {
    @Query("SELECT * FROM ContactInfo")
    fun getAll(): MutableList<ContactInfo>

    @Insert(entity = ContactInfo::class, onConflict = OnConflictStrategy.REPLACE)
    fun insert(contact: ContactInfo?)

    @Query("SELECT * FROM ContactInfo WHERE number=:number")
    fun getContactInfoByNumber(number: String): ContactInfo

}