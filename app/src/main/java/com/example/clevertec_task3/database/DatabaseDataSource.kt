package com.example.clevertec_task3.database

import javax.inject.Inject

class DatabaseDataSource @Inject constructor(private val database: AppDatabase) {
    private val contactsDao = database.contactsDao()

    fun getAll(): MutableList<ContactInfo> {
        return contactsDao.getAll()
    }

    fun insert(weather: ContactInfo) {
        contactsDao.insert(weather)
    }

    fun getContactInfoByNumber(number: String): ContactInfo {
        return contactsDao.getContactInfoByNumber(number)
    }

}