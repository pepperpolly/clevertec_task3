package com.example.clevertec_task3.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ContactsApp : Application() {
    companion object {
        lateinit var app: ContactsApp
    }
}