package com.example.clevertec_task3.di

import android.content.Context
import androidx.room.Room
import com.example.clevertec_task3.database.AppDatabase
import com.example.clevertec_task3.database.DatabaseDataSource
import com.example.clevertec_task3.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

private const val DATABASE_NAME = "ContactInfo"

@Module
@InstallIn(SingletonComponent::class)
class MainModule {
    @Provides
    @Singleton
    fun provideRepo(
        databaseDataSource: DatabaseDataSource
    ): Repository {
        return Repository(
            databaseDataSource
        )
    }

    @Provides
    @Singleton
    fun provideWeatherDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
            .build()
    }
}