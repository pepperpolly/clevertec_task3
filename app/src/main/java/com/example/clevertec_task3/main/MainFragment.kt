package com.example.clevertec_task3.main

import android.Manifest.permission.READ_CONTACTS
import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenStarted
import com.example.clevertec_task3.ContactInfoContract
import com.example.clevertec_task3.R
import com.example.clevertec_task3.database.ContactInfo
import com.example.clevertec_task3.databinding.MainFragmentBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import kotlin.random.Random

@AndroidEntryPoint
class MainFragment : Fragment() {

    private val viewModel by viewModels<MainViewModel>()
    private lateinit var binding: MainFragmentBinding
    private val activityLauncher = registerForActivityResult(ContactInfoContract()) { result ->
        if (result != null) {
            getContacts(result)
        } else {
            Toast.makeText(requireContext(), "Canceled", Toast.LENGTH_SHORT).show()
        }
    }
    private var activityResultLauncher: ActivityResultLauncher<String> = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) {
            activityLauncher.launch(Unit)
        } else {
            Toast.makeText(requireContext(), "Permission denied...", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.whenStarted {
                viewModel.contact.observe(viewLifecycleOwner) {
                    createNotification(
                        "name: ${it.name}\nfamily name: ${it.familyName}\nnumber: ${it.number}\nemail: ${it.emailAddress}"
                    )
                }
            }
        }

        binding.firstOption.setOnClickListener {
            if (checkPermission()) activityLauncher.launch(Unit)
            else activityResultLauncher.launch(READ_CONTACTS)
        }
        binding.secondOption.setOnClickListener {
            openDialogFragment()
        }
        binding.thirdOption.setOnClickListener {
            readFromSP()?.let { Snackbar.make(view, it, Snackbar.LENGTH_SHORT).show() }
        }
        binding.forthOption.setOnClickListener {
            viewModel.getContactInfoByNumber(readFromSP())
        }
    }

    private fun createNotification(text: String) {

        val channelId = "weatherApp"

        val notificationBuilder = NotificationCompat.Builder(requireContext(), channelId)
            .setSmallIcon(R.drawable.ic_baseline_account_circle_24)
            .setContentTitle("Contact info")
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(text)
            )

        val notificationManager =
            requireContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Default Channel",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(Random.nextInt(), notificationBuilder.build())
    }

    private fun readFromSP(): String? {
        val prefs =
            requireContext().getSharedPreferences("numbers", Context.MODE_PRIVATE)
        return prefs.getString("number", "no number")
    }

    private fun openDialogFragment() {
        NumbersDialogFragment().show(childFragmentManager, "numbers")
    }

    private fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireActivity(),
            READ_CONTACTS
        ) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("Range")
    fun getContacts(data: Intent?) {
        val contact = ContactInfo("", null, null, null)
        val uri = data?.data
        val firstCursor =
            uri?.let { requireActivity().contentResolver.query(it, null, null, null, null) }
        if (firstCursor?.moveToFirst() == true) {
            val contactId =
                firstCursor.getString(firstCursor.getColumnIndex(ContactsContract.Contacts._ID))
            val idResults =
                firstCursor.getString(firstCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))
            if (idResults.toInt() == 1) {
                val secondCursor = requireActivity().contentResolver.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                    null,
                    null
                )
                val thirdCursor = requireActivity().contentResolver.query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                    null,
                    null
                )
                val whereName =
                    ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + " = " + contactId
                val whereNameParams =
                    arrayOf(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                val nameCur = requireActivity().contentResolver.query(
                    ContactsContract.Data.CONTENT_URI,
                    null,
                    whereName,
                    whereNameParams,
                    ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME
                )
                if (secondCursor?.moveToFirst() == true) {
                    contact.number =
                        secondCursor.getString(secondCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                }
                if (thirdCursor?.moveToFirst() == true) {
                    contact.emailAddress =
                        thirdCursor.getString(thirdCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                }
                if (nameCur?.moveToFirst() == true) {
                    contact.name =
                        nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME))
                    contact.familyName =
                        nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME))
                }
                secondCursor?.close()
                thirdCursor?.close()
                nameCur?.close()
            }
            firstCursor.close()
        }
        viewModel.insert(contact)
        Toast.makeText(requireContext(), "Success.", Toast.LENGTH_SHORT).show()
    }

}