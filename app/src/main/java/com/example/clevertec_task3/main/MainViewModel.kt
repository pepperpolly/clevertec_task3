package com.example.clevertec_task3.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.clevertec_task3.database.ContactInfo
import com.example.clevertec_task3.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    val contact: MutableLiveData<ContactInfo> = MutableLiveData()

    fun insert(contact: ContactInfo) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insert(contact)
        }
    }

    fun getContactInfoByNumber(number: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            if (number != null) contact.postValue(repository.getContactInfoByNumber(number))
        }
    }

}