package com.example.clevertec_task3.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenStarted
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.clevertec_task3.common.NumberListener
import com.example.clevertec_task3.databinding.NumbersDialogFragmentBinding
import com.example.clevertec_task3.recycler.NumbersAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NumbersDialogFragment : DialogFragment(), NumberListener {

    private lateinit var binding: NumbersDialogFragmentBinding
    private val viewModel by viewModels<NumbersDialogViewModel>()
    private val numbersAdapter = NumbersAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = NumbersDialogFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAllContacts()
        binding.numbersRecycler.adapter = numbersAdapter
        binding.numbersRecycler.layoutManager = LinearLayoutManager(requireContext())
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.whenStarted {
                viewModel.allContacts.observe(viewLifecycleOwner) { it1 ->
                    numbersAdapter.submitList(it1.map { it.number })
                }
            }
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.whenStarted {
                viewModel.contact.observe(viewLifecycleOwner) { it ->
                    Toast.makeText(
                        requireContext(),
                        "name: ${it.name}\nfamily name: ${it.familyName}\nnumber: ${it.number}\nemail: ${it.emailAddress}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    override fun saveNumber(number: String) {
        val settings =
            requireContext().getSharedPreferences("numbers", Context.MODE_PRIVATE)
        val editor = settings.edit()
        editor.putString("number", number)
        editor.apply()
        viewModel.getContactInfoByNumber(number)
        dismiss()
    }
}