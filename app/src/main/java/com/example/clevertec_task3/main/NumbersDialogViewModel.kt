package com.example.clevertec_task3.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.clevertec_task3.database.ContactInfo
import com.example.clevertec_task3.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NumbersDialogViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    val allContacts: MutableLiveData<MutableList<ContactInfo>> = MutableLiveData()
    val contact: MutableLiveData<ContactInfo> = MutableLiveData()

    fun getAllContacts() {
        viewModelScope.launch(Dispatchers.IO) {
            allContacts.postValue(repository.getAll())
        }
    }

    fun getContactInfoByNumber(number: String) {
        viewModelScope.launch(Dispatchers.IO) {
            contact.postValue(repository.getContactInfoByNumber(number))
        }
    }

}