package com.example.clevertec_task3.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.example.clevertec_task3.common.NumberListener
import com.example.clevertec_task3.databinding.NumberItemBinding

class NumbersAdapter(private val listener: NumberListener) :
    ListAdapter<String, NumbersViewHolder>(NumbersDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumbersViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = NumberItemBinding.inflate(layoutInflater, parent, false)
        return NumbersViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: NumbersViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }
}