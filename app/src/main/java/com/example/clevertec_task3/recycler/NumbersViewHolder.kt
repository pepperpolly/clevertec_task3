package com.example.clevertec_task3.recycler

import androidx.recyclerview.widget.RecyclerView
import com.example.clevertec_task3.common.NumberListener
import com.example.clevertec_task3.databinding.NumberItemBinding

class NumbersViewHolder(
    private val binding: NumberItemBinding,
    private val listener: NumberListener
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(number: String) {
        binding.text1.text = number
        binding.root.setOnClickListener {
            listener.saveNumber(number)
        }
    }
}