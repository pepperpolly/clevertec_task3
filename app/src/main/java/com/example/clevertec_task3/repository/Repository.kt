package com.example.clevertec_task3.repository

import com.example.clevertec_task3.database.ContactInfo
import com.example.clevertec_task3.database.DatabaseDataSource
import javax.inject.Inject

class Repository @Inject constructor(
    private val databaseDataSource: DatabaseDataSource
) {
    fun getAll(): MutableList<ContactInfo> {
        return databaseDataSource.getAll()
    }

    fun insert(weather: ContactInfo) {
        databaseDataSource.insert(weather)
    }

    fun getContactInfoByNumber(number: String): ContactInfo {
        return databaseDataSource.getContactInfoByNumber(number)
    }

}